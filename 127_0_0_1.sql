-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2016, 23:45
-- Wersja serwera: 10.1.9-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `symfony`
--
CREATE DATABASE IF NOT EXISTS `symfony` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `symfony`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cat`
--

CREATE TABLE `cat` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `cat`
--

INSERT INTO `cat` (`id`, `parent_id`, `name`) VALUES
(1, NULL, 'Programowanie'),
(2, NULL, 'Grafika'),
(3, 1, 'Java'),
(4, 1, 'C++'),
(5, 1, 'C#'),
(6, 3, 'Spring'),
(7, 2, '2D '),
(8, 7, 'Wektorowa'),
(9, 7, 'Rastrowa'),
(10, 6, 'Security');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(4, 'Programowanie'),
(5, 'Webmasterka'),
(6, 'Administracja');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `author_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published_at` datetime NOT NULL,
  `offer_author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `comment`
--

INSERT INTO `comment` (`id`, `offer_id`, `content`, `author_email`, `published_at`, `offer_author`, `file`) VALUES
(1, 6, 'asdasdasdasdasdasdasdasdasd', 'admin@admin.pl', '2015-12-28 00:27:46', '', ''),
(2, 6, 'asdasdasdasdasdasdasdasdasd', 'admin@admin.pl', '2015-12-28 00:28:29', '', ''),
(3, 6, 'asdasdasdasdasdasdasdasdasd', 'admin@admin.pl', '2015-12-28 00:29:02', '', ''),
(4, 6, 'asdasdasd kasda sd', 'callowiec@gmail.com', '2015-12-28 00:46:08', '', ''),
(5, 6, 'sdsd', 'admin@admin.pl', '2015-12-28 00:47:36', '', ''),
(6, 6, 'eqweqwewe', 'dejna@admin.pl', '2015-12-28 00:49:42', '', ''),
(7, 3, 'czxczxczxc', 'dejna@admin.pl', '2015-12-28 21:22:45', '', ''),
(8, 6, 'asdasdsd', 'dejna@admin.pl', '2015-12-29 21:13:03', 'admin@admin.pl', ''),
(9, 4, 'ddsdffff', 'dejna@admin.pl', '2016-01-02 17:29:49', 'admin@admin.pl', ''),
(10, 4, 'sss', 'admin@admin.pl', '2016-01-02 17:30:02', 'admin@admin.pl', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `author_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published_at` datetime NOT NULL,
  `sended_replay` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `offer`
--

INSERT INTO `offer` (`id`, `category_id`, `title`, `slug`, `content`, `author_email`, `published_at`, `sended_replay`, `type`, `price`, `to_date`) VALUES
(3, 5, 'webmasterka', 'webmasterka', 'jakast tams', 'admin@admin.pl', '2015-12-20 22:55:47', 0, '', '', '0000-00-00 00:00:00'),
(4, 4, 'kuwra jego mac', 'kuwra-jego-mac', 'jakis tam posciskasds', 'callowiec@gmail.com', '2015-12-20 22:55:18', 5, '', '', '0000-00-00 00:00:00'),
(5, 4, 'post i oferta', 'post-i-oferta', 'post i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i oferta', 'admin@admin.pl', '2015-12-22 23:46:48', 0, '', '', '0000-00-00 00:00:00'),
(6, 4, 'post i oferta post i oferta', 'post-i-oferta-post-i-oferta', 'post i ofertapost i ofertapost i ofertapost i ofertapost i oferta', 'admin@admin.pl', '2015-12-22 23:46:55', 0, '', '', '0000-00-00 00:00:00'),
(7, 4, 'oferta i post', 'oferta-i-post', 'oferta i postoferta i postoferta i postoferta i postoferta i post', 'admin@admin.pl', '2015-12-22 23:47:03', 0, '', '', '0000-00-00 00:00:00'),
(8, 4, 'kuwra jego mac', 'kuwra-jego-mac2', 'jakis tam posciskasds', 'admin@admin.pl', '2015-12-20 22:55:18', 0, '', '', '0000-00-00 00:00:00'),
(9, 5, 'webmasterka', 'webmasterka', 'jakast tams', 'admin@admin.pl', '2015-12-20 22:55:47', 0, '', '', '0000-00-00 00:00:00'),
(10, 4, 'kuwra jego mac', 'kuwra-jego-mac', 'jakis tam posciskasds', 'admin@admin.pl', '2015-12-20 22:55:18', 0, '', '', '0000-00-00 00:00:00'),
(11, 4, 'post i oferta', 'post-i-oferta', 'post i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i ofertapost i oferta', 'admin@admin.pl', '2015-12-22 23:46:48', 0, '', '', '0000-00-00 00:00:00'),
(12, 4, 'post i oferta post i oferta', 'post-i-oferta-post-i-oferta', 'post i ofertapost i ofertapost i ofertapost i ofertapost i oferta', 'admin@admin.pl', '2015-12-22 23:46:55', 0, '', '', '0000-00-00 00:00:00'),
(13, 4, 'oferta i post', 'oferta-i-post', 'oferta i postoferta i postoferta i postoferta i postoferta i post', 'admin@admin.pl', '2015-12-22 23:47:03', 0, '', '', '0000-00-00 00:00:00'),
(14, 4, 'kuwra jego mac dwa', 'kuwra-jego-mac-dwa', 'adasdasd asdasdasdasd', 'admin@admin.pl', '2015-12-27 13:10:41', 0, '', '', '0000-00-00 00:00:00'),
(15, 4, 'cos nowego kruwa', 'cos-nowego-kruwa', 'masdaisdasiu dbnasudb asda sdjnasjkdasd', 'dejna@admin.pl', '2015-12-27 15:57:57', 0, '', '', '0000-00-00 00:00:00'),
(16, 4, 'POważna oferta', 'powa-na-oferta', 'Cos adasdasdasd', 'callowiec@gmail.com', '2016-01-04 00:15:37', 0, 'Oferta Pracy', '101.22', '0000-00-00 00:00:00'),
(19, 4, 'Serwiss www do obsługi zawodów sportowych', 'serwiss-www-do-obs-ugi-zawod-w-sportowych', '<p>Treść ogłoszenia:</p>\r\n\r\n<h3>Witam wszystkich ekspertów.</h3>\r\n\r\nPoszukujemy teamu który stworzyłby dla nas serwis www który posiadałby następujące funkcje podzielone na główne kategorie:\r\n\r\n1. Wirtualne Biuro Zawodów:\r\n\r\n- możliwość umieszczania informacji o wydarzeniach sportowych (zawody, treningi, seminaria) przez organizatorów. Tutaj prawdopodobnie będzie się znajdował komponent pobierania opłat (np. 5 PLN od każdego zarejestrowanego zawodnika).\r\n\r\n- możliwość zapisywania się na zawody przez użytkowników (odpłatne lub nieodpłatne)\r\n\r\n- strona główna dla każdego zarejestrowanego użytkownika pokazująca nadchodzące wydarzenia w różnych kategoriach oraz informacje na temat wydarzeń zapisanych, do opłacenia bądź na liście oczekującej.\r\n\r\n- organizatorzy mogą wybrać sposób płatności za zawody - on-line poprzez portal lub poprzez przelew. W przypadku przelewu organizator powinien mieć możliwość wpisywania osób na listę startową manualnie. W przypadku przekroczenia maksymalnej ilości użytkowników tworzona jest lista rezerwowa.\r\n\r\nSerwis powinien być jak najprostszy w obsłudze oraz niezawodny. Platformy z których korzystaliśmy do tej pory potrafiły się zawiesić kiedy 100 użytkowników starało sie zapisać na zawody chwilę po opublikowaniu informacji. Powinien posiadać możliwość rejestracji poprzez FB oraz zapisywanie podstawowych informacji o zawodnikach (profile zawodników) tak, aby użytkownicy nie musieli ich wpisywać od nowa za każdym razem gdy rejestrują się na wydarzenie.\r\n\r\n2. Program do Obsługi Zawodów\r\n\r\n- prosty program do użytku podczas samych zawodów z wbudowanymi podstawowymi zasadami niszowego sportu na którym skupiamy się na początku (psie sporty). Program ten musi obsłużyć listę zgłoszonych zawodników, czasy przebiegów, przewinienia oraz ew. dyskwalifikacje.\r\n\r\n- Możliwość ustalenia czasu maksymalnego i naliczenia punktów karnych za jego przekroczenie.\r\n\r\n- możliwość podziału jednych zawodów na rożne rodzaje biegów oraz startujących psów (klasa small, medium i large).\r\n\r\n- możliwość wyliczania średnich czasów ze wszystkich przebiegów w danej kategorii oraz punktacji łącznej dla poszczególnych zawodników.\r\n\r\n- Program powinien mieć możliwość generowania list startowych, tablic wyników gotowych, dyplomów dla uczestników oraz wygranych (gotowych do wydruku). Dobrze by było gdyby istniała możliwość drobnego ingerowania w wygląd drukowanych dokumentów (np. wstawienie logo organizatora, kilka templatów dyplomów do wyboru).\r\n\r\n- Możliwość eksportowania wyników z programu na stronę www opisaną w pkt. 1. przypisując do danego wydarzenia.\r\n\r\nTak naprawdę zasada działania programu jest prostsza niż wydaje się z opisu. Punktacje są poprostu sumowane i sortowane wg prostej zasady - wygrywa najlepszy czas z najmniejszą ilością przewinień. Czynnikiem komplikującym jest duża ilość zawodników (średnio 150) oraz 3 klasy wzrostowe dla każdej kategorii biegu.\r\n\r\n\r\n3. Komponent portalu dla fotografów sportowych:\r\n\r\n- możliwość uploadowania zdjęć przez użytkowników oraz tagowania ich nazwiskiem, imieniem psa i/lub numerem startowym.\r\n\r\n- możliwość ściągnięcia zdjęć przez pozostałych zawodników w mniejszej rozdzielczość lub ze znakiem wodnym oraz ściągnięcia fotografii w oryginalnej rozdzielczości za opłatą.\r\n\r\nOpisane powyżej funkcje odwołują się do tylko jednego sportu, na którym przetestujemy nasz pomysł. Powinna istnieć prosta metoda umieszczenia dodatkowych rodzajów sportu na serwisie a także dostosowanie formularzy zgłoszeniowych.\r\n\r\nRozumiem doskonale że powyższe infromacje to tylko wierzchołek góry lodowej, ale chcielibyśmy poprosić o wstępne oferty. Zwycięzcę wybierzemy na podstawie ceny, doświadczenia oraz kontaktu osobistego.\r\n\r\nCelem jest uruchomienie serwisu w maju.\r\n\r\nPozdrawiamy,\r\n\r\nEkipa UniqDog.', 'callowiec@gmail.com', '2016-01-04 01:12:50', 0, 'Zlecenie projektu', '40000', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `roles`) VALUES
(3, 'dejna', 'dejna@admin.pl', '$2y$13$FPnSWE1FAM5diwbMEBKgWOKZjjh3Lbq9IBddzmZxgZrCpSnHzrnxO', '["ROLE_USER"]'),
(4, 'admin', 'callowiec@gmail.com', '$2y$13$ksx4zxbbdD.0XzKovylCK.j1kwbfXI0UcSekYBW8uBTszz3UEwDjG', '["ROLE_ADMIN"]'),
(9, 'testowy', 'testowy@gmail.com', '$2y$13$CsXSY6/LoV9v2Iyhl/9VM.tLEDEKgn2ZbHTP.Bty6hZpH6CIK.AXm', '["ROLE_USER"]');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `cat`
--
ALTER TABLE `cat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9E5E43A8727ACA70` (`parent_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526C53C674EE` (`offer_id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_F06D397057698A6A` (`role`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_29D6873E12469DE2` (`category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `cat`
--
ALTER TABLE `cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `cat`
--
ALTER TABLE `cat`
  ADD CONSTRAINT `FK_9E5E43A8727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `cat` (`id`);

--
-- Ograniczenia dla tabeli `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C53C674EE` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`);

--
-- Ograniczenia dla tabeli `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `FK_29D6873E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
