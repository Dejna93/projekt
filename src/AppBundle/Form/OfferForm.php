<?php
/**
 * Created by PhpStorm.
 * User: mce
 * Date: 14.12.15
 * Time: 00:10
 */

namespace AppBundle\Form;

use AppBundle\Form\Type\OfferType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OfferForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title',null,array(
                'label'=> 'Tytuł'))
            ->add('type' , 'choice' , array(
                'choices' => array(
                    'Zlecenie projektu' => 'Zlecenie projektu',
                    'Oferta Pracy' => 'Oferta Pracy',
                ),
            ))
            ->add('price' , 'money' , array(
                'currency' =>'PLN',
                'label'=> 'Cena/Stawka'))

            ->add('toDate', 'date', [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',

                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy',

                ]
            ])
            ->add('content', 'textarea' ,array(
            'label'=> 'Ogłoszenie'))

            ->add('category' , 'entity', array(
                'required' => true,
                'class' => 'AppBundle\Entity\Category',
                'query_builder'=> function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->groupBy('c.id')
                        ->orderBy('c.id', 'ASC');
                }

            ))
            ->add('authorEmail','email' , array(
                'label'=> 'Email' , 'disabled'=>'disabled'))
            ->add('Dodaj', 'submit')

            ;

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' =>'AppBundle\Entity\Offer',
        ));
    }
    public function getName()
    {
        // TODO: Implement getName() method.
        return 'app_offer';
    }

}