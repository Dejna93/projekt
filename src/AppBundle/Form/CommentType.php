<?php
/**
 * Created by PhpStorm.
 * User: callo
 * Date: 27-12-2015
 * Time: 20:25
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;
/*
const textType = 'Symfony\Component\Form\Extension\Core\Type\FileType';*/

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
           ->setAction($options['path'])
            ->setMethod('POST')
            ->add('content', 'textarea' ,array(
                'label'=> 'Ogłoszenie'))
            ->add('file','file' , array('label' => 'CV' , 'required'=>false))
            ->add('submit', 'submit', array('label' => 'Oferta'))

        ;

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' =>'AppBundle\Entity\Comment',
            'path' => null,

        ));
    }
    public function getName()
    {
        // TODO: Implement getName() method.
        return 'app_comment';
    }
}