<?php
/**
 * Created by PhpStorm.
 * User: callo_000
 * Date: 02.11.2015
 * Time: 02:20
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('login', 'text', array('required'=>true));
        $builder->add('password', 'text', array('required'=>true));
        $builder->add('email', 'text', array('required'=>true));

        $builder->add('add_client', 'submit');


  }

    public function getName()
    {
        // TODO: Implement getName() method.
        return 'user';
    }


}