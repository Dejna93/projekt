<?php
/**
 * Created by PhpStorm.
 * User: callo
 * Date: 02-01-2016
 * Time: 15:51
 */

namespace AppBundle\Form;



use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ReplayForm extends AbstractType
{



    /**
     * ReplayForm constructor.
     * @param $token
     */



    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->setMethod('POST')
            ->setAction($options['to'])
            ->add('content', 'textarea' ,array(
                'label'=> 'Ogłoszenie'))
            ->add('submit', 'submit', array('label' => 'Oferta'))

        ;

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'to' => null,
        ));
    }
    public function getName()
    {
        // TODO: Implement getName() method.
        return 'app_replay';
    }

}