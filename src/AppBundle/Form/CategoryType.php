<?php
/**
 * Created by PhpStorm.
 * User: callo
 * Date: 15-12-2015
 * Time: 22:42
 */

namespace AppBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name',null,array(
                'label'=> 'Nazwa'))
        ->add('trushcollector','hidden',array(
            'required'=> false))

        ;

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' =>'AppBundle\Entity\Category',
        ));
    }
    public function getName()
    {
        // TODO: Implement getName() method.
        return 'app_category';
    }
}