<?php
/**
 * Created by PhpStorm.
 * User: callo_000
 * Date: 02.11.2015
 * Time: 22:50
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('email' , 'email')
            ->add('username', 'text')
            ->add('password', 'repeated', array(
                'first_name' => 'pass',
                'second_name' => 'confirm',
                'type' => 'password' ,
                'invalid_message' => 'Passwords do not match'
            ))
 ;

    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class'=> 'AppBundle\Entity\User'
        ));
    }



    public function getName()
    {
        // TODO: Implement getName() method.
        return 'user';
    }


}