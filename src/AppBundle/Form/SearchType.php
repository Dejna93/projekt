<?php
/**
 * Created by PhpStorm.
 * User: callo
 * Date: 24-12-2015
 * Time: 15:43
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($options['path'])
            ->setMethod('POST')
            ->add('search',null)
            ->add('submit','submit',array('label'=> ' '));
    }


    public function getName()
    {
        // TODO: Implement getName() method.
        return 'app_search';
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'path' => null,

        ));
    }

}