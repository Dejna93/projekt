<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2016-01-16
 * Time: 21:22
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            //->setAction($options['path'])
            ->setMethod('POST')
            ->add('title' , null , array('label'=>'Temat'))
            ->add('content', 'textarea' ,array(
                'label'=> 'Wiadomość'))

            ->add('submit', 'submit', array('label' => 'Wyślij'))

        ;

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'path' => null,

        ));
    }
    public function getName()
    {
        // TODO: Implement getName() method.
        return 'app_comment';
    }
}