<?php

/**
 * Created by PhpStorm.
 * User: callo
 * Date: 03-01-2016
 * Time: 23:44
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OfferType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'z' => 'Zlecenie',
                'o' => 'Oferta pracy',
            )
        ));
    }
    public function getName()
    {
        // TODO: Implement getName() method.
        return 'offer_type';
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}