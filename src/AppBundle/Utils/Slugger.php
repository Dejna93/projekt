<?php
/**
 * Created by PhpStorm.
 * User: mce
 * Date: 14.12.15
 * Time: 21:44
 */

namespace AppBundle\Utils;


class Slugger
{
    public function slugify($string){
        return trim(preg_replace('/[^a-z0-9]+/','-',strtolower(strip_tags($string))),'-');
    }

}