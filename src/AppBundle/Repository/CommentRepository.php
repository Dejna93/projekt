<?php
/**
 * Created by PhpStorm.
 * User: callo
 * Date: 28-12-2015
 * Time: 00:31
 */

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;

class CommentRepository extends  EntityRepository
{
    public function findComments($id){
        return $this->getEntityManager()->getRepository('AppBundle:Comment')
            ->createQueryBuilder('comment')
            ->select('c')
            ->from('AppBundle:Comment', 'c')
            ->where('c.offer = :offer')
            ->setParameter('offer' , $id)
            ->getQuery()
            ->getResult();
    }

   public function findByEmail($email){

       return $this->getEntityManager()
           ->getRepository('AppBundle:Comment')
           ->createQueryBuilder('comment')
           ->select('c')
           ->from('AppBundle:Comment' , 'c')
           ->where('c.authorEmail = :email')
           ->setParameter('email', $email)
           ->getQuery()
           ->getResult();

   }

    public function findByUser($email){

        return $this->getEntityManager()->getConnection()
            ->query('SELECT COMMENT.id ,  offer.title , comment.content , comment.author_email , comment.published_at, comment.offer_author FROM comment INNER JOIN offer')->fetchAll();
    }
}