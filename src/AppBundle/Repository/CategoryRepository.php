<?php
/**
 * Created by PhpStorm.
 * User: callo
 * Date: 15-12-2015
 * Time: 22:39
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findByCategory(Category $category)
    {

        return $this->getEntityManager()->getRepository('AppBundle:Offer')->createQueryBuilder('offer')
            ->select('o')
            ->from('AppBundle:Offer', 'o')
            ->where('o.category = :id')->setParameter('id', $category)->getQuery()->getResult();
    }
    public function findId($category){
        return $this->getEntityManager()->getRepository('AppBundle:Offer')
            ->createQueryBuilder('category')
            ->select('c.id')
            ->from('AppBundle:Category', 'c')
            ->where('c.name = :category')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult();
    }


}