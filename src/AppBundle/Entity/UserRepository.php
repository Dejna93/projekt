<?php
/**
 * Created by PhpStorm.
 * User: callo_000
 * Date: 02.11.2015
 * Time: 18:45
 */

namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        // TODO: Implement loadUserByUsername() method.

        $query = $this
            ->createQueryBuilder('u')
            ->select('u,q')
            ->leftJoin('u.groups' , 'g')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username' , $username)
            ->setParameter('email' , $username)
            ->getQuery();

        if($query == null){
            $message = sprintf('Unable to find user %s', $username);
            throw new UsernameNotFoundException($message);
        }
        return $query;
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
        $class = get_class($user);
        if(!$this->supportsClass($class)){
            throw new UnsupportedUserException(
                sprintf('Instance of $s arent supported')

            );

        }
        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        // TODO: Implement supportsClass() method.
        return $this->getEntityName() === $class ||
        is_subclass_of($class , $this->getEntityName());
    }


}