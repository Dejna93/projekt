<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="Offer",  mappedBy="category")
     */
    private $offers;

    /**
     * @ORM\Column(type="boolean")
     */
    private $trushcollector;

    public function __construct(){
        $this->offers = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Offer $offer
     *
     * @return Category
     */
    public function addOffer(\AppBundle\Entity\Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Offer $offer
     */
    public function removeOffer(\AppBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get ads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    public function __toString()
    {
        return $this->getName();
        // TODO: Implement __toString() method.
    }


    /**
     * Set trushcollector
     *
     * @param boolean $trushcollector
     *
     * @return Category
     */
    public function setTrushcollector($trushcollector)
    {
        $this->trushcollector = $trushcollector;

        return $this;
    }

    /**
     * Get trushcollector
     *
     * @return boolean
     */
    public function getTrushcollector()
    {
        return $this->trushcollector;
    }
}
