<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Comment;

/**
 * Comment controller.
 *
 * @Route("/message")
 */
class CommentController extends Controller
{

    /**
     * Lists all Comment entities.
     *
     * @Route("/", name="panel_message_index")
     * @Method("GET")

     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $token = $this->get('security.token_storage')->getToken();
        $user = $token->getUser();

        $c = $em->getRepository('AppBundle:Comment')->findByUser($user->getEmail());

        $entities = $em->getRepository('AppBundle:Comment')->findByEmail($user->getEmail());

       /* $message = \Swift_Message::newInstance()
            ->setSubject('Hello Email')
            ->setFrom('aghitmarket@gmail.com')
            ->setTo('callowiec@gmail.com')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'base.html.twig',
                    array('name' => 'text')
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);*/



        return $this->render('admin/comment/index.html.twig' , array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Comment entity.
     *
     * @Route("/{id}", name="message_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        return array(
            'entity'      => $entity,
        );
    }
}
