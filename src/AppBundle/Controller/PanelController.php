<?php
/**
 * Created by PhpStorm.
 * User: callo_000
 * Date: 08.11.2015
 * Time: 22:42
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Category;
use AppBundle\Entity\Offer;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use AppBundle\Form\MessageType;
use AppBundle\Form\OfferForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Swift_Message;

/**
 * Class PanelController
 * @Route("/panel")

 */
class PanelController extends Controller
{

    /**
 * Lists all Offer entities.
 *
     * @Route ("/" , name="panel_index")
     * @Route ("/" , name="panel_offer_index")
     * @Method("GET")

 */
    public function indexAction()
    {
        $auth_check = $this->get('security.authorization_checker');
        $em = $this->getDoctrine()->getManager();

        if($auth_check->isGranted('ROLE_ADMIN'))
        {
        $entities = $em->getRepository('AppBundle:Offer')->findAll();

        }else if ($auth_check->isGranted('ROLE_USER')){
            $token = $this->get('security.token_storage')->getToken();
            $user = $token->getUser();
           $entities = $em->getRepository('AppBundle:Offer')->findByUser($user->getEmail());


        }else{
            throw new AccessDeniedException('ZAKAZ');
        }
       return $this->render('admin/offer/index.html.twig',array(
            'offers' => $entities,
        ));
    }

    /**
     * @Route("/new", name="panel_offer_new")
     * @Method({"GET","POST"})

     */
    public function newAction(Request $request){
        $offer = new Offer();

        $offer->setAuthorEmail($this->getUser()->getEmail());
        $form = $this->createForm(new OfferForm(),$offer);

        $form->handleRequest($request);

        if($form->isSubmitted() ){
            $offer->setSlug($this->get('slugger')->slugify($offer->getTitle()));
            $entityManager = $this->getDoctrine()->getManager();



            $category = $entityManager->getRepository('AppBundle:Category')->find($offer->getCategory()->getId());

            $offer->setCategory($category);
            $entityManager->persist($offer);
            $entityManager->flush();

            $this->addFlash('success','Pomyślnie dodano');
            echo 'succes';
            if($form->get('Dodaj')->isClicked()){
                return $this->redirectToRoute('panel_offer_new');
            }
            return $this->redirectToRoute('panel_offer_index');
        }

        return $this->render('admin/offer/new.html.twig',array(
            'offer'=>$offer,
            'form' =>$form->createView(),
        ));
    }

    /**
     * Finds and displays a Offer entity.
     *
     * @Route("/{id}", name="panel_offer_show" , requirements={"id" : "\d+"})
     * @Method("GET")
     **/
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Offer')->find($id);
        $comments = $em->getRepository('AppBundle:Comment')->findComments($entity->getId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('admin/offer/show.html.twig', array(
            'offer'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'comments' => $comments,

        ));
    }

    /**
     * @Route("/{id}/edit",name="panel_offer_edit")
     * @Method("GET")
     */
    public function editAction($id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Offer')->find($id);

        if(!$entity){
            throw $this->createNotFoundException('Unable find some data');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('admin/offer/edit.html.twig',array(
            'offer'=>$entity,
            'edit_form'=> $editForm,
            'delete_form'=>$deleteForm,
        ));
    }

    /**
     * Creates a form to edit a Offer entity.
     *
     * @param Offer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Offer $entity)
    {
        $form = $this->createForm(new OfferForm(), $entity, array(
            'action' => $this->generateUrl('panel_offer_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }
    /**
     * Edits an existing Offer entity.
     *
     * @Route("/{id}", name="panel_offer_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Offer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('panel_offer_edit', array('id' => $id)));
        }

        return $this->render( 'admin/offer/edit.html.twig'  ,array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Offer entity.
     *
     * @Route("/{id}", name="panel_offer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Offer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Offer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('panel_offer_index');
    }

    /**
     * Creates a form to delete a Offer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('panel_offer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }



    /**
     * @Route("/comment/{id}/new" , name="panel_comment_add")
     * @Method("GET")
     */

    public function commentNewAction(Request $request,$id){

        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('AppBundle:Comment')->find($id);

        $replay = new Comment();
        $replay->setOfferAuthor($comment->getAuthorEmail());

        $form = $this->createForm(new CommentType(),null);
        return $this->render('admin/comment/new.html.twig',
            array('form' => $form->createView(),
                'comment' => $comment)
        );
    }

    /**
     * @Route("/meesages", name="panel_messages")
     * @Method("GET")
     */
    public function messageAction(Request $request){

        $form = $this->createForm(new MessageType(),null,array('path'=>$this->generateUrl('panel_messages_send')));

        $form->handleRequest($request);


        return $this->render('admin/user/message.html.twig',
            array(
                'form_comment' =>$form->createView(),
                'user' =>''
            ));
    }
    /**
     * @Route("/meesages", name="panel_messages_send")
     * @Method("POST")
     */

    public function sendAction(Request $request){

        $form = $this->createForm(new MessageType());
        $form->handleRequest($request);

        if($form->isSubmitted()){

            $data = $form->getData();
            $token = $this->get('security.token_storage')->getToken();
            $user = $token->getUser();

            $mail = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();


            $message = Swift_Message::newInstance()
                ->setSubject($data['title'])
                ->setFrom($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'mails/replay.html.twig',
                        array('user' => $user->getEmail(),
                            'content'=>$data['content'],
                        )
                    ),
                    'text/html'
                )
            ;

            foreach($mail as $val){
                $message->addCC($val->getEmail());
            }

            if($this->get('mailer')->send($message)){
                $request->getSession()->getFlashBag()->add('success','Wiadomość została wysłana');
            }else{
                $request->getSession()->getFlashBag()->add('error','Wiadomość nie została wysłana');
            }



            return $this->redirectToRoute('panel_messages');
        }
        return $this->redirectToRoute('panel_offer_index');


    }






}