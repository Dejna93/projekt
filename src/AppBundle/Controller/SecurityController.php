<?php
/**
 * Created by PhpStorm.
 * User: mce
 * Date: 13.12.15
 * Time: 23:27
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{



    /**
     * @Route("/login", name="security_login_form")
     *
     */
    public function loginAction(){
        $utils = $this->get('security.authentication_utils');

        return $this->render('security/login.html.twig',
            array('last_username' => $utils->getLastUsername(),
                'error' => $utils->getLastAuthenticationError(),));
    }

    /**
     * @Route("/login_check" , name="security_login_check")
     */
    public function loginCheckAction(){
        throw new \Exception('You shall not pass ');
    }

    /**
     * @Route("/logout",name="security_logout")
     */
    public function logoutAction(){
        throw new \Exception('You shall not pass ');
    }
}