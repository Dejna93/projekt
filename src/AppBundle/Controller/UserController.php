<?php

namespace AppBundle\Controller;

use AppBundle\Form\MessageType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Response;
use Swift_Message;

/**
 * User controller.
 *
 * @Route("/panel")
 *  @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends Controller
{

    /**
     * @Route("/users", name="panel_user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:User')->findAll();
        $entity = new User();

        return $this->render( 'admin/user/index.html.twig'  ,array(
           'entities' => $entities,
        ));
    }
    /**
     * @Route("/users/add", name="panel_user_add")
     * @Method("GET")
     */
    public function addAction(Request $request){

        return $this->redirectToRoute('register_form');


    }
    /**
     * @Route("/users/{id}/message", name="panel_user_message")
     * @Method("GET")
     */
    public function messageAction(Request $request,$id){
        $form = $this->createForm(new MessageType(),null,array('path'=>$this->generateUrl('panel_user_send',array('id' => $id ))));

        $form->handleRequest($request);
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        return $this->render('admin/user/message.html.twig',
            array(
                'form_comment' =>$form->createView(),
                'user'=> $user,
            ));
    }
    /**
     * @Route("/users/{id}/message", name="panel_user_send" ,requirements={ "id": "\d+"})
     * @Method("POST")
     */
    public function sendAction(Request $request,$id){

        $form = $this->createForm(new MessageType());

        $form->handleRequest($request);

        if($form->isSubmitted()){

            $data = $form->getData();
            $token = $this->get('security.token_storage')->getToken();
            $user = $token->getUser();

            $mail = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
                $message = Swift_Message::newInstance()
                    ->setSubject($data['title'])
                    ->setFrom($user->getEmail())
                    ->setTo($mail->getEmail())
                    ->setBody(
                        $this->renderView(
                            'mails/replay.html.twig',
                            array('user' => $user->getEmail(),
                                'content'=>$data['content'],
                            )
                        ),
                        'text/html'
                    )
                ;
            if($this->get('mailer')->send($message)){
                $request->getSession()->getFlashBag()->add('success','Wiadomość została wysłana');
            }else{
                $request->getSession()->getFlashBag()->add('error','Wiadomość nie została wysłana');
            }



            return $this->redirectToRoute('panel_user_message');
        }
        return $this->redirectToRoute('panel_user_index');

    }
    /**
     *
     * Creates a new User entity.
     *
     * @Route("/", name="panel_user_create")
     * @Method("POST")
     * @Template("AppBundle:User:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panel_user_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('panel_user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }


    /**
     * Finds and displays a User entity.
     *
     * @Route("/users/{id}", name="panel_user_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/users/edit/{id}", name="panel_user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('panel_user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     * @Route("/users/{id}", name="panel_user_update")
     * @Method("PUT")
     * @Template("AppBundle:User:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('panel_user_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a User entity.
     *
     * @Route("/users/{id}", name="panel_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panel_user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('panel_user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
