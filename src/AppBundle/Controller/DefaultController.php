<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Form\ReplayForm;
use AppBundle\Form\SearchType;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User;
use AppBundle\Entity\Offer;
use AppBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="offer_index")
     * @Route("/page/{page}" , name="index_paginated" , requirements={"page" : "\d+"})
     *
     */
    public function indexAction(Request $page)
    {
        $token = $this->get('security.token_storage')->getToken();
        $user = $token->getUser();

        $searchForm = $this->createForm(new SearchType(),null,array('path'=>$this->generateUrl('offer_search')));


        $query = $this->getDoctrine()->getRepository('AppBundle:Offer')->findOffers();

        $em = $this->getDoctrine()->getManager();


        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $query = array_reverse($query);
        $paginator = $this->get('knp_paginator');

        $paginator = $paginator->paginate(
            $query , $page->query->getInt('page',1), 10
        );

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $sumreplay = $em->createQuery('SELECT SUM(o.sendedReplay)
          FROM AppBundle:Offer o
           WHERE o.authorEmail = :email')
                ->setParameter('email', $user->getEmail())->getArrayResult();

            $countoffer = $em->createQuery('SELECT COUNT(o.id)
          FROM AppBundle:Offer o
           WHERE o.authorEmail = :email')
                ->setParameter('email', $user->getEmail())->getArrayResult();


            return $this->render('offers/index.html.twig',array(
                'pagination' =>$paginator ,
                'categories' => $categories,
                'search_form' => $searchForm->createView(),

                'sumreplay' => $sumreplay[0][1],
                'countoffer' => $countoffer[0][1],
            ));
        }

        //0000000000

        return $this->render('offers/index.html.twig',array(
            'pagination' =>$paginator ,
            'categories' => $categories,
            'sumreplay' => 0,
            'countoffer' => 0,
        ));


        // replace this example code with whatever you need


    }

    /**
     * @Route("/category" , name="category_offer")
     */
    public function categoryAction(Request $request){
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        return $this->render('offers/category.html.twig', array('categories' =>$category));
    }

    /**
     * @Route("/category/{id}" , name="category_offer_show")
     * @Route("/page/{page}" , name="index_paginated" , requirements={"page" : "\d+"})
     */
    public function showCategoryAction(Request $page ,$id){

        $em = $this->getDoctrine()->getManager();

        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        $offers = $this->getDoctrine()->getRepository('AppBundle:Offer')->findByCategory($id);
        $offers  = array_reverse($offers );

        $paginator = $this->get('knp_paginator');

        $paginator = $paginator->paginate(
            $offers , $page->query->getInt('page',1), 10
        );


        return $this->render('offers/index.html.twig' , array('pagination'=>$paginator,'categories'=>$categories) );
    }

    /**
     * @Route("/offers/{id}" , name="offer_post")
     */
    public function offerShowAction(Offer $offer){

        $em = $this->getDoctrine()->getManager();


        $form = $this->createForm(new CommentType(),new Comment(),array( 'path' => $this->generateUrl('comment_new',array('id' => $offer->getId() ))));




        return $this->render('offers/post_show.html.twig',
            array('offer' => $offer,
                'form_comment' =>$form->createView(),


            ));
    }

    /**
     * @Route("/comment/{id}", name = "comment_new")

     *
     * @Method("POST")
     *
     */
    public function commentNewAction(Request $request ,Offer $offer )
    {

       $form = $this->createForm(new CommentType());

        $form->handleRequest($request);

        if($form->isSubmitted()){

           $comment = $form->getData();
            $file = $comment->getFile();

            /* $comment->setAuthorEmail($this->getUser()->getEmail());
            $comment->setOffer($offer);
            $comment->setOfferAuthor($offer->getAuthorEmail());
            ;*/
            $token = $this->get('security.token_storage')->getToken();
            $user = $token->getUser();
            if($file != null){
                $message = Swift_Message::newInstance()
                    ->setSubject('Replay for yours offer ' + $offer->getTitle())
                    ->setFrom($user->getEmail())
                    ->setTo($offer->getAuthorEmail())
                    ->attach($file)
                    ->setBody(
                        $this->renderView(
                            'mails/replay.html.twig',
                            array('user' => $user->getEmail(),
                                'content'=>$comment->getContent(),
                            )
                        ),
                        'text/html'
                    )
                ;
            }else{
                $message = Swift_Message::newInstance()
                    ->setSubject('Replay for yours offer ' + $offer->getTitle())
                    ->setFrom($user->getEmail())
                    ->setTo($offer->getAuthorEmail())
                    ->setBody(
                        $this->renderView(
                            'mails/replay.html.twig',
                            array('user' => $user->getEmail(),
                                'content'=>$comment->getContent(),
                            )
                        ),
                        'text/html'
                    )
                ;
            }


            $this->get('mailer')->send($message);

            $request->getSession()->getFlashBag()->add('success','Wiadomość została wysłana');

            $em = $this->getDoctrine()->getManager();
            $offered = $em->getRepository('AppBundle:Offer')->find($offer->getId());
            $offered->setSendedReplay($offered->getSendedReplay() + 1 );
            $em->persist($offered);
            $em->flush();
            return $this->redirectToRoute('offer_post' , array('id' =>$offer->getId()));


        }
        return $this->redirectToRoute('offer_index');
    }

    /**
     * @Route("/comment/{id}/replay" , name="comment_replay")
     * @Method("POST")
     */
    public function commentReplayAction(Request $request){

        $form = $this->createForm(new ReplayForm());

        $form->handleRequest($request);

       if($request->isMethod('POST'))
       {

           $form->submit($request);

          print_r($form->getData());
            return new Response($form->get('content')->getData());
       }


       return  $this->redirectToRoute('offer_index');


    //  $form->handleRequest($request);

       /* if($form->isSubmitted()){

            echo 'kurwa';
            return $this->render('');
        }*/

    }


    /**
     * @Route("/register", name="register_form")
     */
    public function registerAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(new UserType(),$user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){

            $user->setRoles(array('ROLE_USER'));
            $encode = $this->get('security.password_encoder');
            $encodedPassword = $encode->encodePassword($user,$user->getPassword());
            $user->setPassword($encodedPassword);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('offer_index');

        }

          return $this->render('security/register.html.twig',
        array('form'=> $form->createView(),));
    }


    /**
     * @Route("/search" , name="offer_search")
     * @Method("POST")
     */
    public function searchAction(Request $request)
    {
        $form = $this->createForm(new SearchType());

        $form->handleRequest($request);



        if($form->isSubmitted()) {

            $data = $form->getData();
            $search = $data['search'];

            $em = $this->getDoctrine()->getManager();
           $result= $em->createQuery('SELECT o FROM AppBundle:Offer o  WHERE o.title LIKE :search or o.content LIKE :search')
                ->setParameter('search',"%$search%")->getArrayResult();

        $offers = new Offer();
            foreach($result as $offer){

                $offers = $em->getRepository('AppBundle:Offer')->find($offer['id']);
                print_r(  $offers);
            }

            $categories = $em->getRepository('AppBundle:Category')->findAll();


            /*$paginator = $this->get('knp_paginator');

            $paginator = $paginator->paginate(
                $offers , $request->query->getInt('page',1), 10
            );*/
            $searchForm = $this->createForm(new SearchType(),null,array('path'=>$this->generateUrl('offer_search')));
            return $this->render('offers/index.html.twig',array(
                'pagination' =>$offers ,
                'categories' => $categories,
                'search_form' => $searchForm->createView(),

                'sumreplay' =>'',
                'countoffer' => '',
            ));
        }

        return new Response('s');
       // return $this->redirectToRoute("offer_index");

    }







}
